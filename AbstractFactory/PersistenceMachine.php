<?php
namespace Padroes\AbstractFactory;
use Padroes\AbstractFactory\Repositories\Repository1;
use Padroes\AbstractFactory\Repositories\Repository2;
use Padroes\AbstractFactory\Repositories\Repository3;
use Padroes\AbstractFactory\Repositories\Repository4;

interface PersistenceMachine {
    public function getRepository1() : Repository1;
    public function getRepository2() : Repository2;
    public function getRepository3() : Repository3;
    public function getRepository4() : Repository4;
}
