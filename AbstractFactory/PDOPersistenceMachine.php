<?php

namespace Padroes\AbstractFactory;
use Padroes\AbstractFactory\PersistenceMachine;
use Padroes\AbstractFactory\Repositories\PDO\PDORepository1;
use Padroes\AbstractFactory\Repositories\PDO\PDORepository2;
use Padroes\AbstractFactory\Repositories\PDO\PDORepository3;
use Padroes\AbstractFactory\Repositories\PDO\PDORepository4;
 
class PDOPersistenceMachine implements PersistenceMachine {
    private $conexao;
    
    public function __construct() {
        
    }
    
    public function getRepository1(): Repository1 {
        return new PDORepository1();
    }

    public function getRepository2(): Repository2 {
        return new PDORepository2();
    }

    public function getRepository3(): Repository3 {
        return new PDORepository3();
    }

    public function getRepository4(): Repository4 {
        return new PDORepository4();
    }

}
