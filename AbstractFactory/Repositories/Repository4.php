<?php
namespace Padroes\AbstractFactory\Repositories;

interface Repository1 {
    public function getById(int $id);
    public function getByNome(string $nome);
}
