<?php
namespace Padroes\AbstractFactory\Repositories;

interface Repository4 {
    public function getById(int $id);
    public function getByNome(string $nome);
}
