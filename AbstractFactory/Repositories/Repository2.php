<?php
namespace Padroes\AbstractFactory\Repositories;

interface Repository2 {
    public function getById(int $id);
    public function getByNome(string $nome);
}
