<?php

namespace Padroes\FactoryMethod;

use Padroes\FactoryMethod\ServiceFactory;
use Padroes\FactoryMethod\ServiceDefault;

class ServiceDefaultFactory implements ServiceFactory {
    
    public function getInstance() : Service {
        return new ServiceDefault();
    }

}

