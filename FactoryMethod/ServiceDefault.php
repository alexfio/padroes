<?php

namespace Padroes\FactoryMethod;

use Padroes\FactoryMethod\Service;


class ServiceDefault implements Service {
    
    public function method1() {
        echo "method 1";
    }

    public function method2() {
        echo "method 2";
    }

    public function method3() {
        echo "method 3";
    }

}



