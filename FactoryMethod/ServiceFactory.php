<?php

namespace Padroes\FactoryMethod;
use Padroes\FactoryMethod\Service;

interface ServiceFactory {
    public function getInstance() : Service;
}

