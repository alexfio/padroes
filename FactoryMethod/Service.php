<?php

namespace Padroes\FactoryMethod;

interface Service {
    public function method1();
    public function method2();
    public function method3();
}

