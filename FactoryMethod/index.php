<?php

require_once "../vendor/autoload.php";

use Padroes\FactoryMethod\ServiceFactory;
use Padroes\FactoryMethod\ServiceDefaultFactory;

function executarFuncao(ServiceFactory $serviceFactory) {
    $service = $serviceFactory->getInstance();
    var_dump($service);
}

$factory = new ServiceDefaultFactory();

executarFuncao($factory);

