<?php

namespace Padroes\Singleton;

require_once "../vendor/autoload.php";

use Padroes\Singleton\ServiceDefault;

$service = ServiceDefault::getInstance();
$service2 = ServiceDefault::getInstance();

$service->getDado();
$service2->getDado();
