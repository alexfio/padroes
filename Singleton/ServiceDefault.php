<?php

namespace Padroes\Singleton;

class ServiceDefault {
    private static $instance;
    private $dado;
    
    private function __construct() {
        $this->dado = "Dado";
    } 
    
    public static function getInstance() {
        if(isset(self::$instance)) {
            return self::$instance;
        } 
        
        self::$instance = new self(); 
        return self::$instance; 
    } 
    
    public function getDado() {
        var_dump($this);
        return $this->dado;
    }
    
}



